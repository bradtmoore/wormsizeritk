For a visual demonstration, see the [ video ]( ./WormSizerITK.mp4 )

The goal of this demo is to demonstrate my ability to use and extend the ITK framework.  I decided
to take my previous application WormSizer (https://github.com/bradtmoore/wormsizer) and mirror
its algorithm in ITK.  WormSizer is an ImageJ/Fiji application.

WormSizer segments worms from brightfield microscopy images.  It automatically segments the images and
returns morphological measurements for each worm detected.  These include length (by medial axis) and
width.  By assuming each worm is round, WormSizer also computes a 3D approximate volume for each worm.

I decided to implement the basic algorithm in ITK, but instead of just returning the volume measurement,
I have used ITK to construct a 3D mesh of each worm.

To limit the scope of this demo, I have focused on segmenting a single image (test4.png).  Besides the
segmentation, the rest of the pipeline is parameter-less.

Requirements
======================

This application requires ITK 4.10.0 as well as (https://www.geometrictools.com/) GTEngine v140.  It also
requires GoogleTest.