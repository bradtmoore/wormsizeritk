#ifndef GTENGINEBINARYTHINNINGFILTER_H
#define GTENGINEBINARYTHINNINGFILTER_H

#include "itkImageToImageFilter.h"
#include "itkImage.h"

using GTEngineImage = itk::Image<unsigned char, 2>;

class GTEngineBinaryThinningFilter : public itk::ImageToImageFilter< GTEngineImage, GTEngineImage >
{
public:
    typedef GTEngineBinaryThinningFilter Self;
    typedef itk::ImageToImageFilter< GTEngineImage, GTEngineImage > Superclass;
    typedef itk::SmartPointer< Self > Pointer;

    itkNewMacro(Self);

    itkTypeMacro(GTEngineImage, itk::ImageToImageFilter);

protected:
    GTEngineBinaryThinningFilter();
    ~GTEngineBinaryThinningFilter();

    virtual void GenerateData();

private:
    GTEngineBinaryThinningFilter(const Self&);
    void operator=(const Self&);
};

#endif // GTENGINEBINARYTHINNINGFILTER_H
