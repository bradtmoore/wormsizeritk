#ifndef INDEXHASH_H
#define INDEXHASH_H

#include "itkIndex.h"

///
/// A hash function for an itk::Index object (e.g. for use with std::map)
///
template <typename TIndex>
struct IndexHash
{
    std::size_t operator()(TIndex const& s) const
    {
        // stole this from boost, but I didn't want to bring the whole library as a dependence for a one-liner function
        // seed ^= hash_value(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2)
        unsigned int n = TIndex::Dimension;
        std::size_t seed = 0;
        for (unsigned int i = 0; i < n; ++i) {
            std::size_t h = std::hash<TIndex::IndexValueType>{}(s[i]);
            seed ^= h + 0x9e3779b9 + (seed << 6) + (seed >> 2);
        }

        return seed; // or use boost::hash_combine (see Discussion)
    }
};

//namespace std {

//  template <>
//  struct hash< itk::Index< 2 > >
//  {
//    std::size_t operator()(const itk::Index< 2 >& s) const
//    {
//        // stole this from boost, but I didn't want to bring the whole library as a dependence for a one-liner function
//        // seed ^= hash_value(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2)
//        unsigned int n = 2;
//        std::size_t seed = 0;
//        for (unsigned int i = 0; i < n; ++i) {
//            std::size_t h = std::hash< itk::Index< 2 >::IndexValueType>{}(s[i]);
//            seed ^= h + 0x9e3779b9 + (seed << 6) + (seed >> 2);
//        }

//        return seed; // or use boost::hash_combine (see Discussion)
//    }
//  };

//}

#endif // INDEXHASH_H
