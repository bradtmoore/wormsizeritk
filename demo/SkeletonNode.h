#ifndef SKELETONNODE_H
#define SKELETONNODE_H

#include <vector>
#include <memory>

///
/// Represents a node of a medial axis.  It contains a 2D index/point of the medial axis, a type
/// (EDGE or ENDPOINT), and pointers to adjacent nodes.
///
template < typename TIndex >
class SkeletonNode
{
public:
    ///
    /// \brief this node is connected to two other points (middle of the medial axis)
    ///
    static const int EDGE = 1;

    ///
    /// \brief this node is connected to one other point (end of the medial axis)
    ///
    static const int ENDPOINT = 2;

    typedef TIndex IndexType;
    typedef std::shared_ptr< SkeletonNode > SharedPointer;

    SkeletonNode(IndexType index);
    ~SkeletonNode();

    ///
    /// \brief returns the type of this node
    /// \return
    ///
    int GetType();

    ///
    /// \brief returns the location of this node
    /// \return
    ///
    const IndexType GetIndex() const;

    ///
    /// \brief Adds an adjacent node to this node, updates the type accordingly
    /// \param neighbor
    ///
    void AddNeighbor(SharedPointer neighbor);

    ///
    /// \brief Move to the next point (for ENDPOINT nodes)
    /// \return
    ///
    SharedPointer Follow();

    ///
    /// \brief Move to the next point (for EDGE nodes)
    /// \param prev the previous node (how we came here, determines the direction of movement)
    /// \return
    ///
    SharedPointer Follow(SkeletonNode &prev);

    ///
    /// \brief Nodes are equal if their location is equal
    /// \param rhs
    /// \return
    ///
    bool operator==(const SkeletonNode &rhs) const;
    bool operator!=(const SkeletonNode &rhs) const;

protected:
    int type;
    const IndexType index;
    std::vector< SharedPointer > neighbors;
};

template < typename TIndex >
SkeletonNode< TIndex >::SkeletonNode(IndexType index) : index(index) {

}

template < typename TIndex >
SkeletonNode< TIndex >::~SkeletonNode() {

}

template < typename TIndex >
int SkeletonNode< TIndex >::GetType() {
    return type;
}

template < typename TIndex >
const typename SkeletonNode< TIndex >::IndexType SkeletonNode< TIndex >::GetIndex() const {
    return index;
}

template < typename TIndex >
void SkeletonNode< TIndex >::AddNeighbor(SharedPointer neighbor) {
    neighbors.push_back(neighbor);
    type = (neighbors.size() == 1) ? ENDPOINT : EDGE;
}

template < typename TIndex >
typename SkeletonNode< TIndex >::SharedPointer SkeletonNode< TIndex >::Follow() {
    return neighbors.at(0);
}

template < typename TIndex >
typename SkeletonNode< TIndex >::SharedPointer SkeletonNode< TIndex >::Follow(SkeletonNode &prev) {
    SharedPointer ans;
    for (SharedPointer n : neighbors) {
        if (*n != prev) {
            ans = n;
            break;
        }
    }
    return ans;
}

template < typename TIndex >
bool SkeletonNode< TIndex >::operator==(const SkeletonNode &rhs) const {
    return rhs.index == index;
}

template < typename TIndex >
bool SkeletonNode< TIndex >::operator!=(const SkeletonNode &rhs) const {
    return ! (rhs == *this);
}

#endif // SKELETONNODE_H
